DBA.define('MainHeader', ['jQuery', 'Response'], function($, Response) {

    'use strict';
    //dom-elements
    var $mainMenu = $('.main-menu');
    var $burgerBtn = $('.main-header__burger-btn');

    //classes
    var $mainMenuActiveClass = 'main-menu--active';

    // constructor
    function MainHeader ($el) {
        console.log('*** MainHeader ***');
        this.initialize();
    }
    // public methods and variables
    MainHeader.prototype.initialize = function initialize() {
        MainHeader.prototype.menuToggleActions();
    };

    MainHeader.prototype.menuToggleActions = function menuToggleActions() {
      $burgerBtn.on('click', function($el){
        $el.preventDefault();
        if ($mainMenu.hasClass($mainMenuActiveClass)) {
            $mainMenu.removeClass($mainMenuActiveClass);
            $(window).disablescroll("undo");
        } else {
          $mainMenu.addClass($mainMenuActiveClass);
           $(window).disablescroll();
        }
      });
    }
    // register as view
    this.registerView('MainHeader', MainHeader);
    // always return constructor if you define a 'class'
    return MainHeader;
});

DBA.define('MainMenu', ['jQuery', 'Response'], function($, Response) {

    'use strict';

    //dom-elements
    var $subMenu = $('.main-menu__sub-navigation');
    var $menuItem = $('.main-menu__navigation-item__link');

    //classes
    var $menuItemActiveClass = 'main-menu__navigation-item__link--active';
    var $subMenuActiveClass = 'main-menu__sub-navigation--active';

    // constructor
    function MainMenu ($el) {
        console.log('*** MainMenu ***');
        this.initialize();
    }

    // public methods and variables
    MainMenu.prototype.initialize = function initialize() {
        MainMenu.prototype.menuToggleActions();
    };

    MainMenu.prototype.menuToggleActions = function menuToggleActions() {
      $menuItem.on('mouseover', function($el){
        $el.preventDefault();
        if ($(this).hasClass($menuItemActiveClass)) {
          $(this).removeClass($menuItemActiveClass);
          $(this).siblings($subMenu).removeClass($subMenuActiveClass);
        } else {
          $menuItem.removeClass($menuItemActiveClass);
          $(this).addClass($menuItemActiveClass);
          $subMenu.removeClass($subMenuActiveClass);
          $(this).siblings($subMenu).addClass($subMenuActiveClass);
        }
      });
    }

    // register as view
    this.registerView('MainMenu', MainMenu);
    //
    // always return constructor if you define a 'class'
    return MainMenu;
});

DBA.define('HeroSection', ['jQuery', 'Response'], function($, Response) {

    'use strict';
    // constructor
    function HeroSection ($el) {
        console.log('*** HeroSection ***');
    }
    // register as view
    this.registerView('HeroSection', HeroSection);
    //
    // always return constructor if you define a 'class'
    return HeroSection;
});

DBA.define('CoursesSection', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function CoursesSection ($el) {
		console.log('*** CoursesSection ***');

		$('.item__header').flip({
			axis: 'y',
			trigger: 'hover',
			reverse: true
		});

		var owl = $("#owl-main-carousel");

		owl.owlCarousel({
			items : 3, //10 items above 1000px browser width
			itemsDesktop : [1000,2], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,2], // betweem 900px and 601px
			itemsTablet: [600,1], //2 items between 600 and 0
			itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
			autoPlay : 5000,
			stopOnHover : true,
			autoHeight : true
		});
		
		// // Custom Navigation Events
		// $(".next").click(function(){
		// 	owl.trigger('owl.next');
		// })
		// $(".prev").click(function(){
		// 	owl.trigger('owl.prev');
		// })
		// $(".play").click(function(){
		// 	owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		// })
		// $(".stop").click(function(){
		// 	owl.trigger('owl.stop');
		// })
	}
	// register as view
	this.registerView('CoursesSection', CoursesSection);
	//
	// always return constructor if you define a 'class'
	return CoursesSection;
});

DBA.define('VideoSection', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function VideoSection ($el) {
		console.log('*** VideoSection ***');

		var owl3 = $("#owl-main-carousel3");

		owl3.owlCarousel({
			autoPlay : 4000,
			stopOnHover : true,
			autoHeight : true,
			singleItem : true,
			transitionStyle : "fadeUp"
		});
		
		// // Custom Navigation Events
		$(".video-section__navigation__right").click(function(){
			owl3.trigger('owl.next');
		})
		$(".video-section__navigation__left").click(function(){
			owl3.trigger('owl.prev');
		})
		// $(".play").click(function(){
		// 	owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		// })
		// $(".stop").click(function(){
		// 	owl.trigger('owl.stop');
		// })
	}
	// register as view
	this.registerView('VideoSection', VideoSection);
	//
	// always return constructor if you define a 'class'
	return VideoSection;
});


DBA.define('StepsSection', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function StepsSection ($el) {
		console.log('*** StepsSection ***');

		var owl2 = $("#owl-steps-carousel2");

		owl2.owlCarousel({
			items : 3, //10 items above 1000px browser width
			itemsDesktop : [1000,2], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,1], // betweem 900px and 601px
			itemsTablet: [600,1], //2 items between 600 and 0
			itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
			autoPlay : 5000,
			stopOnHover : true,
			autoHeight : true
		});

	}
	// register as view
	this.registerView('StepsSection', StepsSection);
	//
	// always return constructor if you define a 'class'
	return StepsSection;
});

DBA.define('MapComponent', ['jQuery', 'Response'], function($, Response) {

  'use strict';
  //global Vars
  var map = {};
  var marker = [];
  var london = new google.maps.LatLng(51.5072, -0.1);

  // var aberdeen = new google.maps.LatLng(57.149717, -2.094278);
  // var reading = new google.maps.LatLng(51.454265, -0.978130);
  // var manchester = new google.maps.LatLng(53.480759, -2.242631);
  // var birmingham = new google.maps.LatLng(52.486243, -1.890401);


  var cities = [
    new google.maps.LatLng(57.149717, -2.094278),
    new google.maps.LatLng(51.454265, -0.978130),
    new google.maps.LatLng(53.480759, -2.242631),
    new google.maps.LatLng(52.486243, -1.890401)
  ];

  var aberdeen = cities[0];
  var reading = cities[1];
  var manchester = cities[2];
  var birmingham = cities[3];

  // constructor
  function MapComponent($el) {
    console.log('*** MapComponent ***');
    google.maps.event.addDomListener(window, 'load', this.initialize);
  };

  MapComponent.prototype.initialize = function initialize() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
      center: new google.maps.LatLng(54.673351, -11.674101),
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.ROAD,
      draggable: false,
      scrollwheel: false,
      panControl: false,
      disableDefaultUI: true
    };
    map = new google.maps.Map(mapCanvas, mapOptions);

    google.maps.event.addDomListener(window, "resize", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
      MapComponent.prototype.clearMarkers();
    });

    MapComponent.prototype.dropMarkers();
    MapComponent.prototype.slider();
  };


  MapComponent.prototype.dropMarkers = function dropMarkers(position) {
    MapComponent.prototype.clearMarkers();
    MapComponent.prototype.addMarker(position);
  };

  MapComponent.prototype.addMarker = function addMarker(position) {
    marker.push(new google.maps.Marker({
      position: position,
      map: map,
      draggable: false,
      animation: google.maps.Animation.DROP
    }));
  };

  MapComponent.prototype.clearMarkers = function clearMarkers() {
    for (var i = 0; i < marker.length; i++) {
      marker[i].setMap(null);
    }
    marker = [];
  };

  MapComponent.prototype.slider = function Slider() {
    $.fn.scrollList = function(delay) {
      delay = delay || 5000; //Default delay value can be specified
      var limit = 8; //number of items to show - 100px margin-top (6 shown 5 etc.)
      var totalHeight = 0;
      var itemHeight = $(this).children('li').outerHeight(true);
      $(this).children().each(function(i, val) {
        if (i > limit) return false; //limiting each function

        totalHeight = totalHeight + $(this).outerHeight(true);
      });
      var overflowHeight = totalHeight - itemHeight;
      $(this).height(overflowHeight);


      var animateList = function($list) {
        var $first = $list.children('li:first');
        $list.css('margin-top', -itemHeight);
        $first.animate({
            'margin-top': itemHeight * 1
          },
          function() {
            $list.children('li:last').prependTo($list); //get last and put it on top of the list
            $(this).css('margin-top', 0); //reset margin value
            var cityName = $(this).data('city');
            var position;
            switch (cityName) {
              case 'Aberdeen':
                position = aberdeen;
                break;
              case 'Reading':
                position = reading;
                break;
              case 'Manchester':
                position = manchester;
                break;
              case 'Birmingham':
                position = birmingham;
                break;
              default:
                'Aberdeen'
            }
            MapComponent.prototype.dropMarkers(position);
            setTimeout(function() {
              animateList($list);
            }, delay);
          });
      };

      return this.each(function() {
        var that = $(this);
        setTimeout(function() {
          animateList(that);
        }, delay);
      });
    };
    $('#verticalCarousel').scrollList();
  };


  // register as view
  this.registerView('MapComponent', MapComponent);
  //
  // always return constructor if you define a 'class'
  return MapComponent;
});

DBA.define('PartnersComponent', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function PartnersComponent ($el) {
		console.log('*** PartnersComponent ***');

		$('.partner-item').flip({
			axis: 'y',
			trigger: 'hover',
			reverse: true
		});
		
	}
	// register as view
	this.registerView('PartnersComponent', PartnersComponent);
	//
	// always return constructor if you define a 'class'
	return PartnersComponent;
});
