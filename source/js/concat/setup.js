/*global _:false */
// create DBA Object
var DBA = {};

// Starting Function
(function () {
    'use strict';

    function id (x) { return x; }
    var mods = {},
        q = [],
        inits;

    // DBA.define
    DBA.define = function define (name, deps, init) {
        function checkDeps (item) {
            var deps = item.deps.map(function (d) { return mods[d]; });

            if (item.init && deps.every(id)) {
                mods[item.name] = item.init.apply(DBA, deps);
                item.init = null;
                inits++;
            }
        }

        q.push({name: name, deps: deps, init: init});
        do {
            inits = 0;
            q.forEach(checkDeps);
        } while (inits);
    };

    DBA.define('Modernizr', [], function () { return Modernizr; });
    DBA.define('jQuery', [], function () { return jQuery; });
    DBA.define('Response', [], function () { return Response; });
    //DBA.define('_', [], function () { return _; });

    // establish breakpoints
    Response.create({
        prop: 'width',  // 'width' 'device-width' 'height' 'device-height' or 'device-pixel-ratio'
        prefix: 'min-width- r src',  // the prefix(es) for your data attributes (aliases are optional)
        breakpoints: [1280, 1024, 0], // min breakpoints (defaults for width/device-width)
        lazy: true // optional param - data attr contents lazyload rather than whole page at once
    });

    // apply placeholder polyfill to input elements
    //$('input, textarea').placeholder();

})();

// DBA.registerView
DBA.define('viewMain', ['jQuery'], function ($) {
    'use strict';

    var views = {};
    var currentView = {};

    DBA.registerView = function (name, fn) { views[name] = fn; };
    $(function () {
        $('*[data-component]').each(function (i, el) {
            var name = $(el).data('component') || '',
                cfg = $(el).data('config') || {};

            if (name) {
                if (views[name]) {
                    currentView = new views[name]($(el), cfg);
                } else {
                    console.warn('view `' + name + '` isn\'t defined');
                }
            } else {
                console.warn('empty data-component!');
            }
        });
    });
    return this;
});
