DBA.define('HeroSection', ['jQuery', 'Response'], function($, Response) {

    'use strict';
    // constructor
    function HeroSection ($el) {
        console.log('*** HeroSection ***');
    }
    // register as view
    this.registerView('HeroSection', HeroSection);
    //
    // always return constructor if you define a 'class'
    return HeroSection;
});
