DBA.define('PartnersComponent', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function PartnersComponent ($el) {
		console.log('*** PartnersComponent ***');

		$('.partner-item').flip({
			axis: 'y',
			trigger: 'hover',
			reverse: true
		});
		
	}
	// register as view
	this.registerView('PartnersComponent', PartnersComponent);
	//
	// always return constructor if you define a 'class'
	return PartnersComponent;
});
