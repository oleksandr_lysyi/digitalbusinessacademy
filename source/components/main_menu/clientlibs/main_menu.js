DBA.define('MainMenu', ['jQuery', 'Response'], function($, Response) {

    'use strict';

    //dom-elements
    var $subMenu = $('.main-menu__sub-navigation');
    var $menuItem = $('.main-menu__navigation-item__link');

    //classes
    var $menuItemActiveClass = 'main-menu__navigation-item__link--active';
    var $subMenuActiveClass = 'main-menu__sub-navigation--active';

    // constructor
    function MainMenu ($el) {
        console.log('*** MainMenu ***');
        this.initialize();
    }

    // public methods and variables
    MainMenu.prototype.initialize = function initialize() {
        MainMenu.prototype.menuToggleActions();
    };

    MainMenu.prototype.menuToggleActions = function menuToggleActions() {
      $menuItem.on('mouseover', function($el){
        $el.preventDefault();
        if ($(this).hasClass($menuItemActiveClass)) {
          $(this).removeClass($menuItemActiveClass);
          $(this).siblings($subMenu).removeClass($subMenuActiveClass);
        } else {
          $menuItem.removeClass($menuItemActiveClass);
          $(this).addClass($menuItemActiveClass);
          $subMenu.removeClass($subMenuActiveClass);
          $(this).siblings($subMenu).addClass($subMenuActiveClass);
        }
      });
    }

    // register as view
    this.registerView('MainMenu', MainMenu);
    //
    // always return constructor if you define a 'class'
    return MainMenu;
});
