DBA.define('MainHeader', ['jQuery', 'Response'], function($, Response) {

    'use strict';
    //dom-elements
    var $mainMenu = $('.main-menu');
    var $burgerBtn = $('.main-header__burger-btn');

    //classes
    var $mainMenuActiveClass = 'main-menu--active';

    // constructor
    function MainHeader ($el) {
        console.log('*** MainHeader ***');
        this.initialize();
    }
    // public methods and variables
    MainHeader.prototype.initialize = function initialize() {
        MainHeader.prototype.menuToggleActions();
    };

    MainHeader.prototype.menuToggleActions = function menuToggleActions() {
      $burgerBtn.on('click', function($el){
        $el.preventDefault();
        if ($mainMenu.hasClass($mainMenuActiveClass)) {
            $mainMenu.removeClass($mainMenuActiveClass);
            $(window).disablescroll("undo");
        } else {
          $mainMenu.addClass($mainMenuActiveClass);
           $(window).disablescroll();
        }
      });
    }
    // register as view
    this.registerView('MainHeader', MainHeader);
    // always return constructor if you define a 'class'
    return MainHeader;
});
