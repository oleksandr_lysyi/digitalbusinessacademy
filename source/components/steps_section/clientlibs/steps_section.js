DBA.define('StepsSection', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function StepsSection ($el) {
		console.log('*** StepsSection ***');

		var owl2 = $("#owl-steps-carousel2");

		owl2.owlCarousel({
			items : 3, //10 items above 1000px browser width
			itemsDesktop : [1000,2], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,1], // betweem 900px and 601px
			itemsTablet: [600,1], //2 items between 600 and 0
			itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
			autoPlay : 5000,
			stopOnHover : true,
			autoHeight : true
		});

	}
	// register as view
	this.registerView('StepsSection', StepsSection);
	//
	// always return constructor if you define a 'class'
	return StepsSection;
});
