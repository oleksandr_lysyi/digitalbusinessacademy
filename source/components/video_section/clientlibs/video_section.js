DBA.define('VideoSection', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function VideoSection ($el) {
		console.log('*** VideoSection ***');

		var owl3 = $("#owl-main-carousel3");

		owl3.owlCarousel({
			autoPlay : 4000,
			stopOnHover : true,
			autoHeight : true,
			singleItem : true,
			transitionStyle : "fadeUp"
		});
		
		// // Custom Navigation Events
		$(".video-section__navigation__right").click(function(){
			owl3.trigger('owl.next');
		})
		$(".video-section__navigation__left").click(function(){
			owl3.trigger('owl.prev');
		})
		// $(".play").click(function(){
		// 	owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		// })
		// $(".stop").click(function(){
		// 	owl.trigger('owl.stop');
		// })
	}
	// register as view
	this.registerView('VideoSection', VideoSection);
	//
	// always return constructor if you define a 'class'
	return VideoSection;
});

