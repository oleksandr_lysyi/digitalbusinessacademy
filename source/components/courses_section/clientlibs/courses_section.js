DBA.define('CoursesSection', ['jQuery', 'Response'], function($, Response) {

	'use strict';
	// constructor
	function CoursesSection ($el) {
		console.log('*** CoursesSection ***');

		$('.item__header').flip({
			axis: 'y',
			trigger: 'hover',
			reverse: true
		});

		var owl = $("#owl-main-carousel");

		owl.owlCarousel({
			items : 3, //10 items above 1000px browser width
			itemsDesktop : [1000,2], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,2], // betweem 900px and 601px
			itemsTablet: [600,1], //2 items between 600 and 0
			itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
			autoPlay : 5000,
			stopOnHover : true,
			autoHeight : true
		});
		
		// // Custom Navigation Events
		// $(".next").click(function(){
		// 	owl.trigger('owl.next');
		// })
		// $(".prev").click(function(){
		// 	owl.trigger('owl.prev');
		// })
		// $(".play").click(function(){
		// 	owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		// })
		// $(".stop").click(function(){
		// 	owl.trigger('owl.stop');
		// })
	}
	// register as view
	this.registerView('CoursesSection', CoursesSection);
	//
	// always return constructor if you define a 'class'
	return CoursesSection;
});
