DBA.define('MapComponent', ['jQuery', 'Response'], function($, Response) {

  'use strict';
  //global Vars
  var map = {};
  var marker = [];
  var london = new google.maps.LatLng(51.5072, -0.1);

  // var aberdeen = new google.maps.LatLng(57.149717, -2.094278);
  // var reading = new google.maps.LatLng(51.454265, -0.978130);
  // var manchester = new google.maps.LatLng(53.480759, -2.242631);
  // var birmingham = new google.maps.LatLng(52.486243, -1.890401);


  var cities = [
    new google.maps.LatLng(57.149717, -2.094278),
    new google.maps.LatLng(51.454265, -0.978130),
    new google.maps.LatLng(53.480759, -2.242631),
    new google.maps.LatLng(52.486243, -1.890401)
  ];

  var aberdeen = cities[0];
  var reading = cities[1];
  var manchester = cities[2];
  var birmingham = cities[3];

  // constructor
  function MapComponent($el) {
    console.log('*** MapComponent ***');
    google.maps.event.addDomListener(window, 'load', this.initialize);
  };

  MapComponent.prototype.initialize = function initialize() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
      center: new google.maps.LatLng(54.673351, -11.674101),
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.ROAD,
      draggable: false,
      scrollwheel: false,
      panControl: false,
      disableDefaultUI: true
    };
    map = new google.maps.Map(mapCanvas, mapOptions);

    google.maps.event.addDomListener(window, "resize", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
      MapComponent.prototype.clearMarkers();
    });

    MapComponent.prototype.dropMarkers();
    MapComponent.prototype.slider();
  };


  MapComponent.prototype.dropMarkers = function dropMarkers(position) {
    MapComponent.prototype.clearMarkers();
    MapComponent.prototype.addMarker(position);
  };

  MapComponent.prototype.addMarker = function addMarker(position) {
    marker.push(new google.maps.Marker({
      position: position,
      map: map,
      draggable: false,
      animation: google.maps.Animation.DROP
    }));
  };

  MapComponent.prototype.clearMarkers = function clearMarkers() {
    for (var i = 0; i < marker.length; i++) {
      marker[i].setMap(null);
    }
    marker = [];
  };

  MapComponent.prototype.slider = function Slider() {
    $.fn.scrollList = function(delay) {
      delay = delay || 5000; //Default delay value can be specified
      var limit = 8; //number of items to show - 100px margin-top (6 shown 5 etc.)
      var totalHeight = 0;
      var itemHeight = $(this).children('li').outerHeight(true);
      $(this).children().each(function(i, val) {
        if (i > limit) return false; //limiting each function

        totalHeight = totalHeight + $(this).outerHeight(true);
      });
      var overflowHeight = totalHeight - itemHeight;
      $(this).height(overflowHeight);


      var animateList = function($list) {
        var $first = $list.children('li:first');
        $list.css('margin-top', -itemHeight);
        $first.animate({
            'margin-top': itemHeight * 1
          },
          function() {
            $list.children('li:last').prependTo($list); //get last and put it on top of the list
            $(this).css('margin-top', 0); //reset margin value
            var cityName = $(this).data('city');
            var position;
            switch (cityName) {
              case 'Aberdeen':
                position = aberdeen;
                break;
              case 'Reading':
                position = reading;
                break;
              case 'Manchester':
                position = manchester;
                break;
              case 'Birmingham':
                position = birmingham;
                break;
              default:
                'Aberdeen'
            }
            MapComponent.prototype.dropMarkers(position);
            setTimeout(function() {
              animateList($list);
            }, delay);
          });
      };

      return this.each(function() {
        var that = $(this);
        setTimeout(function() {
          animateList(that);
        }, delay);
      });
    };
    $('#verticalCarousel').scrollList();
  };


  // register as view
  this.registerView('MapComponent', MapComponent);
  //
  // always return constructor if you define a 'class'
  return MapComponent;
});
