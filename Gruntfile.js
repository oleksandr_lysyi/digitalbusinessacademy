module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // project settings
    project: {
      build: 'build',
      source: 'source',
      preview: 'preview',
      css: 'sass'
    },
    // task settings
    clean: {
      combined_js: {
        files: {
          src: ['<%= project.source %>/concat']
        }
      },
      preview: {
        files: {
          src: ['<%= project.preview %>/**']
        }
      },
      build: {
        files: {
          src: ['<%= project.build %>/**']
        }
      }
    },
    compass: {
      compile: {
        options: {
          httpPath: '<%= project.source %>',
          cssDir: '<%= project.source %>/css',
          sassDir: '<%= project.source %>/sass',
          imagesDir: '<%= project.source %>/img',
          relativeAssets: true
            //force: false
        }
      }
    },
    compress: {
      main: {
        options: {
          archive: 'build.zip'
        },
        files: [{
          expand: true,
          src: '**/*',
          cwd: '<%= project.build %>'
        }]
      }
    },
    // use this task to combine JS files like they were being combined in Ruby
    concat: {
      libs: {
        src: [
          '<%= project.source %>/bower_components/jquery/dist/jquery.min.js',
          '<%= project.source %>/bower_components/bootstrap/dist/bootstrap.min.js',
          '<%= project.source %>/bower_components/underscore/underscore.min.js',
          '<%= project.source %>/bower_components/fastclick/lib/fastclick.js',
          '<%= project.source %>/bower_components/responsejs/response.min.js',
          '<%= project.source %>/js/lib/appendAround.js',
          '<%= project.source %>/js/lib/jquery.disablescroll.min.js',
          '<%= project.source %>/bower_components/flip/src/flip.js',
          '<%= project.source %>/bower_components/jcarousellite/jcarousellite.js',
          '<%= project.source %>/bower_components/owlcarousel/owl-carousel/owl.carousel.min.js'
        ],
        dest: '<%= project.source %>/js/concat/libs.js'
      },
      define: {
        src: [
          '<%= project.source %>/js/app/define.js',
          '<%= project.source %>/js/app/registerView.js'
        ],
        dest: '<%= project.source %>/js/concat/setup.js'
      },
      components: {
        src: [
          '<%= project.source %>/components/main_header/clientlibs/main_header.js',
          '<%= project.source %>/components/main_menu/clientlibs/main_menu.js',
          '<%= project.source %>/components/hero_section/clientlibs/hero_section.js',
          '<%= project.source %>/components/courses_section/clientlibs/courses_section.js',
          '<%= project.source %>/components/video_section/clientlibs/video_section.js',
          '<%= project.source %>/components/steps_section/clientlibs/steps_section.js',
          '<%= project.source %>/components/map_component/clientlibs/map_component.js',
          '<%= project.source %>/components/partners_component/clientlibs/partners_component.js',
        ],
        dest: '<%= project.source %>/js/concat/components.js'
      },
      all: {
        src: [
          '<%= project.source %>/js/concat/libs.js',
          '<%= project.source %>/js/concat/setup.js',
          // need to create DBA object and define and renderView methods here
          '<%= project.source %>/js/concat/components.js'
        ],
        dest: '<%= project.preview %>/js/combined.js' // send combined.js to preview
      },
    },
    connect: {
      server: {
        options: {
          base: '<%= project.preview %>',
          // hostname: '10.22.132.93',
          port: grunt.option('serverPort') || 3000
        }
      }
    },
    copy: {
      html: {
        files: [{
          expand: true,
          flatten: true,
          cwd: '<%= project.source %>',
          src: [
            '*.html',
            'rendered-html/**'
          ],
          dest: '<%= project.preview %>',
          filter: 'isFile'
        }]
      },
      css: {
        files: [{
          expand: true,
          cwd: '<%= project.source %>/css',
          src: '*.css',
          dest: '<%= project.preview %>/css',
          filter: 'isFile'
        }]
      },
      scripts: {
        files: [{
          expand: true,
          cwd: '<%= project.source %>/bower_components/modernizr/',
          src: [
            'modernizr.js'
          ],
          dest: '<%= project.preview %>/js/lib/',
          filter: 'isFile'
        }]
      },
      fonts: {
        files: [{
          expand: true,
          cwd: '<%= project.source %>/fonts/',
          src: '**',
          dest: '<%= project.preview %>/fonts/',
          filter: 'isFile'
        }]
      },
      build: {
        files: [{
          expand: true,
          cwd: '<%= project.preview %>',
          src: '**',
          dest: '<%= project.build %>',
          filter: 'isFile'
        }]
      }
    },
    ejs_static: {
      preview: {
        options: {
          dest: '<%= project.source %>/rendered-html/',
          path_to_data: '<%= project.source %>/data/data.json',
          path_to_layouts: '<%= project.source %>',
          parent_dirs: false,
          underscores_to_dashes: true,
          file_extension: '.html'
        }
      }
    },
    imagemin: {
      dynamic: {
        options: {
          svgoPlugins: [{ removeViewBox: false }],
        },
        files: [{
          expand: true,
          cwd: '<%= project.source %>/img',
          src: [
            '**/*.{png,jpg,gif}',
            '!icons/**',
            '!icons-2x/**',
            '!_from-refresh/**',
            '!**/*.psd'
          ],
          dest: '<%= project.preview %>/img'
        }]
      }
    },
    svgmin: { //minimize SVG files
      options: {
          plugins: [
              { removeViewBox: true },
              { removeUselessStrokeAndFill: true }
          ]
      },
      dist: {
          expand: true,
          cwd: '<%= project.source %>/img',
          src: ['*.svg'],
          dest: '<%= project.preview %>/img'
          //ext: '.colors-light-danger-success.svg'
      }
    },
    watch: {
      styles: {
        files: ['<%= project.source %>/**/*.scss'],
        tasks: ['compass', 'copy:css'],
        options: {
          livereload: true
        }
      },
      scripts: {
        files: [
          '<%= project.source %>/**/*.js',
          '!<%= project.source %>/js/concat/**'
        ],
        tasks: ['concat']
      },
      templates: {
        files: [
          '<%= project.source %>/pages/**',
          '<%= project.source %>/components/**'
        ],
        tasks: ['ejs_static:preview', 'copy:html']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      options: {
        livereload: true
      }
    }
  });

  /**
   * Load Grunt plugins
   */
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-connect-proxy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-ejs-static');
  grunt.loadNpmTasks('grunt-svgmin');

  grunt.registerTask('default', [
    'connect:server',
    'watch'
  ]);

  grunt.registerTask('develop', [
    'clean:combined_js',
    'clean:preview',
    'compass',
    'concat',
    'ejs_static',
    'imagemin',
    'svgmin',
    'copy:html',
    'copy:css',
    'copy:scripts',
    'copy:fonts',
    'connect:server',
    'watch'
  ]);

  grunt.registerTask('build', [
    'clean',
    'compass',
    'concat',
    'ejs_static',
    'imagemin',
    'svgmin',
    'copy'
  ]);
};
